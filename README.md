
<!-- README.md is generated from README.Rmd. Please edit that file -->

# Advent of Code

<!-- badges: start -->

![](https://img.shields.io/badge/2023%20star%20count-jrwinget%208*-green.svg)
<!-- badges: end -->

This repo contains all of my Advent of Code solutions using R. I’m using
this to practice programming and problem-based solutions, but it’s
mostly for a little holiday fun. I’m also using a custom repo badge that
scrapes a user’s Advent of Code star count and displays it as a badge.

## Solutions so far

### [2023](R/2023) \| [2022](R/2022) \| [2021](R/2021) \| [2020](R/2020)

| Day |                        Puzzle                         |      Solution       |                Data                 |
|:---:|:-----------------------------------------------------:|:-------------------:|:-----------------------------------:|
|  1  |  [Trebuchet?!](https://adventofcode.com/2023/day/1)   | [R](R/2023/day01.R) | [Puzzle input](data/2023/day01.txt) |
|  2  | [Cube Conundrum](https://adventofcode.com/2023/day/2) | [R](R/2023/day02.R) | [Puzzle input](data/2023/day02.txt) |
|  3  |  [Gear Ratios](https://adventofcode.com/2023/day/3)   | [R](R/2023/day03.R) | [Puzzle input](data/2023/day03.txt) |
|  4  |  [Scratchcards](https://adventofcode.com/2023/day/4)  | [R](R/2023/day04.R) | [Puzzle input](data/2023/day04.txt) |
